---
layout: default
title: Common challenges
parent: Pattern library
has_children: true
nav_order: 2
---

# Common problems and challenges in free software development