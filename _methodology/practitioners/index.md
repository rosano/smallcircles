---
layout: default
title: Practitioners guide
nav_order: 2
---

# Social Coding in Practice
{: .fs-9 .no_toc }

How do you grow a free software project and form a thriving community that fosters it? Here we collect recipes that help guide practitioners along a path to delivering great results.
{: .fs-6 .fw-500 .ff-rajdhani }

[Discuss Recipes](https://forum.forgefriends.org/c/socialcoding/practitioners-guide/35){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Healthy communities

