<div align="center">

[![social coding logo](assets/images/social-codinglogo.png)](https://coding.social)

### [Project website](https://coding.social) | [Matrix chatroom](https://matrix.to/#/#socialcoding:matrix.org) | [Codeberg organization](https://codeberg.org/SocialCoding)

</div>

<br>

This is Social Coding website. Here we develop a new approach to free software development.

## Contents

- [Usage](#usage)
- [Contributing](#contributing)
- [Development](#development)
- [Publishing](#publishing)
- [Credits](#credits)
- [License](#license)

## Usage

[View the online documentation](https://coding.social/movement/publish-howto/) for detailed usage information on how to publish your own content to the site.

## Contributing

Bug reports and pull requests are welcome. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](https://contributor-covenant.org) code of conduct.

Create an issue in our [Issue Tracker](https://codeberg.org/SocialCoding/website/issues) before you start to work on major changes, and check first if a similar issue doesn't already exist. If you want to add something entirely new, then it might be best to start a [project discussion](https://codeberg.org/SocialCoding/discussion/issues) beforehand.

## Development

Install the prerequisites for building [Jekyll](https://jekyllrb.com/) websites, fork the repository to your Codeberg account and then clone that repository to your local computer.

To set up your environment for site development, run `bundle install`.

To test your theme, run `jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server to preview your changes. Add pages, documents, data, etc. like normal to test your modified contents. As you make modifications to the site and its content, the site will regenerate and you should see the changes in the browser after a refresh..

When done editing and everything looks good, you can commit the changes to your Codeberg fork and create a Pull Request. Make sure that both your commit message and the PR description are succinct, clear and summarize the changes you've made.

If your PR relates to an Issue in the tracker, then mention the issue ID in the PR title between brackets e.g. "Improved accessibility (#11)".

## Publishing

The site maintainers will review your PR and publish to the website when all suggested changes are accepted.

## Credits

The Social Coding website is based on the [Just-the-Docs](https://pmarsceill.github.io/just-the-docs/) Jekyll theme. Many thanks to [Patrick Marsceill](http://patrickmarsceill.com/) and all theme [contributors](https://pmarsceill.github.io/just-the-docs/#thank-you-to-the-contributors-of-just-the-docs), and to [Codeberg](https://codeberg.org) for hosting great FOSS and this website.

### Contributors

See the list of [Contributors](CONTRIBUTORS.md) to the Social Coding website, and don't forget to add yourself to this list if you are one of them.

## License

[AGPL-3.0](LICENSE) for project code, and [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) for documentation.
