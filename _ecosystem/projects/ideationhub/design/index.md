---
layout: default
title: Design
parent: IdeationHub
has_children: true
nav_order: 2
---

# Designing an ideation platform for the Fediverse
{: .no_toc }

## How we move forward

We are only at the start of the adventure to make this project shine. All possibilities are open to us. For the time being we prefer to [stay with ambiguity ](https://dougbelshaw.com/ambiguity/) as much as possible, and we welcome you to join the discussion and participate.

{: .highlight}
> #### Does This Project Inspire You?
>
> We need your [input and help](/movement/about-us) to get the Ideation Hub off the ground.