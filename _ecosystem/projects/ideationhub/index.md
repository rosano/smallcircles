---
layout: default
title: IdeationHub
parent: Projects
has_children: true
nav_order: 2
---

![Ideation Hub logo](/assets/images/ideationhub-logo.svg)

# Brainstorm and materialize ideas together on the Fediverse

## About this project

The Fediverse is a great place to interact with other people, and it is full of creativity. But it is still highly oriented towards microblogging only. When interesting discussions happen it is hard to follow-up to something that is more than 'just talk'. After a short time the thread is mostly forgotten about. Encouragement to fedizens to join some other place to collaborate and take things further are mostly fruitless efforts.

The Ideation Hub project wants to change that. The goal of this project is to make it easier for all fedizens to bring their ideas to life. By facilitating a collaborative ideation process and the subsequent formation of vibrant communities around ideas, the Ideation Hub will foster the establishment of a multitude of Social Coding projects aimed to implement them.

## Get inspired

Before delving any deeper it is _highly recommended_ to watch this excellent video by [Steven Johnson](https://stevenberlinjohnson.com/), the author of the book ["Where Good Ideas Come From"](https://www.penguinrandomhouse.com/books/299687/where-good-ideas-come-from-by-steven-johnson/9781594485381).

<div class="embed-container">
 _ <iframe src="https://yewtu.be/embed/NugRZGDbPFU" frameborder='0' allowfullscreen>
  _</iframe>
</div>

The landscape of ideas is what we explore with the Ideation Hub project, with ideas ranging from mundane to wildly imaginative and radical. And we want to explore them appropriately, in a process based on [Social Coding principles](/principles) that lead to beautiful projects and collectively shaped by many fedizens.

{: .highlight}
> #### IdeationHub Main Objective
>
> "What is the best way to bring our ideas to life?"

### Where good ideas come from

Reformulating from [this review](https://scribe.rip/key-lessons-from-books/the-key-lessons-from-where-good-ideas-come-from-by-steven-johnson-1798e11becdb) by [Blinkist](https://medium.com/u/cb07e371ea2d) of Steven Johnson's book, there are seven key lessons to learn. Note how well they match to what we have on the Fediverse:

1. Evolution depends on all available possibilities.
2. Good ideas generally evolve over longer timespans.
3. Platforms are like springboards for innovations.
4. Innovation and evolution thrive in large networks.
5. Collaboration is an important driver of innovation.
6. Lucky connections between ideas drive innovation.
7. When diverse people meet creative collisions happen.