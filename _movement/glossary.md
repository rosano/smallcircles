---
layout: default
title: Glossary of terms
nav_order: 99
---

## Glossary

### About this page

We keep an up-to-date glossary of all the common terminology that is used throughout our initiative. If you find something that is missing, then suggest an update in our [website issue tracker](https://codeberg.org/SocialCoding/website/issues), or contribute it directly.

### Common terminology

| Term | Explanation |
| :---: | :--- |
| Community of Action | There are [different types](https://en.wikipedia.org/wiki/Category:Types_of_communities) of community. A [Community of Action](https://en.wikipedia.org/wiki/Community_of_action) or CoA is the most desirable type for Social Coding as it is focused on stimulating collective action and broad collaboration. |
| FLO | Acronym that stands for "Free, Libre, Open", a term invented by [Snowdrift](https://snowdrift.coop), which emphasizes the flow of ideas, culture, creativity, and liberties that are possible when we remove artificial restrictions and encourage cooperation. See Snowdrift's [definition of FLO](https://wiki.snowdrift.coop/about/free-libre-open#flo). |


### Ideation & idea management
