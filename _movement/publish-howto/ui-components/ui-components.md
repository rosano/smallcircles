---
layout: default
title: UI Components
parent: Publish to this site
nav_order: 3
has_children: true
permalink: /movement/publish-howto/ui-components
---

# UI Components

To make it as easy as possible to write documentation in plain Markdown, most UI components are styled using default Markdown elements with few additional CSS classes needed.
{: .fs-6 .fw-300 }
