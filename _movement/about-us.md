---
layout: default
title: Join the movement
nav_order: 2
---

# Join the movement and be a social coder

## How we are organized

The Social Coding activities within our own community roughly revolve around three different tracks:

1. [**Community**:](#community-track) Passionate free software lovers that work together.
2. [**Practitioners**:](#practitioners-track) Refine a methodology and collect best-practices.
3. [**Supporters**:](#supporters-track) Build a fediverse platform with helpful tools.

### Community track

We want to gather a movement of fine people who think, act and feel to be Social Coders and want to propagate the field of Social Coding.

### Practitioners track

We want to stimulate the methodology of Social Coding to evolve and be actively used in free software development by technologists and their audience (stakeholders).

### Supporters track

We want to provide the supportive technology and tools to make social coding a fun and useful exercise for any technologist to adopt, as well as for the audience that is served by their work.

## Where you can find us

You can participate with us in many different ways. Our main channels are:

- [Discussion](https://discuss.coding.social/): Discuss and elaborate relevant topics on our co-shared community forum.
- [Chat](https://matrix.to/#/#socialcoding-movement:matrix.org): Join the chatrooms in our Matrix space for some real-time talk.
- [Source code](https://codeberg.org/SocialCoding): Find our code repositories and issue trackers where you can contribute.
- [Fediverse](https://a.gup.pe/u/socialcoding): Follow our group account and find all its @mentions in your timeline.

Our ecosystem communities and projects also have their own contact points to interact directly.

## Who are we?

The Social Coding movement is an initiative that was launched in December 2021 by:

- [Arnold Schrijver](https://mastodon.social/@humanetech)
- [CSDUMMI](https://norden.social/@csddumi)

We started discussing the ideas that you find on this website in the [Fediverse Futures](https://lemmy.ml/c/fediversefutures) community on Lemmy. We copied the thread to our issue tracker: [United Software Development: A New Paradigm?](https://codeberg.org/SocialCoding/discussion/issues/1)