---
layout: default
title: Join the Social Coding Movement
nav_order: 1
nav_exclude: true
description: "Social Coding is participatory free software development that is inclusive to all people and focused on addressing real human needs."
permalink: /
---

# Create solutions that people love
{: .fs-9 .no_toc }

Social Coding is participatory free software development that is inclusive to all people and focused on addressing real human needs. Ideation, design and development is a collective and social effort.
{: .fs-6 .fw-500 .ff-rajdhani }

[Join the Movement](movement/about-us/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Welcome to Social Coding Movement

This movement is dedicated to making free software development more social. We try to bridge the gap that exists between developers and the people they aim to serve, and also address the many challenges that the free software community faces. On this website you will find our:

- [**Practitioners Guide:**](/methodology/practitioners) A guided tour along the development lifecycle.
- [**Pattern Library**](/methodology/patterns) Explore challenges and best ways to mitigate them.
- [**Ecosystem Support**](#social-coding-ecosystem) Affiliated initiatives that support our cause.

If you are a first-time visitor we recommend you read [Why we need Social Coding](/movement/blog/why-social-coding) on our blog.

### Follow CODE Principles

At the heart of Social Coding are four basic principles, and we adhere to them ourself:

1. [**C**ommunity first:](/principles/#community-first) Collaborate with diverse people.
2. [**O**pen is everything:](/principles/#open-is-everything) Work entirely in the open.
3. [**D**emocratic governance:](/principles/#democratic-governance) Give everyone a voice.
4. [**E**ducate and learn:](/principles/#educate-and-learn) Share your experiences freely.

How these principles translate to practices is part of the work we do. An ongoing effort.

### Deliver true GEM's

Three practices are key to the software that Social Coding practitioners strive to deliver:

1. [**G**row affinity:](/principles/#grow-affinity) Focus on people's needs.
2. [**E**volve the commons:](/principles/#evolve-the-commons) Contribute to the common good.
3. [**M**onitor externalities:](/principles/#monitor-externalities) Carefully weigh your impact.

## Social Coding Ecosystem

### Ideation Hub

### Forgefriends Community

### Federated Diversity Foundation

### Delightful Gems

The [Delightful Project]() brings together a set of curated lists of the most delightful FOSS, Open Science and Open Data resources for other people to discover and enjoy. Anyone is free to host their own list on a theme of their interest, and become part of the project.

<br>

{: .important}
> This website is crowdsourced. It can ALWAYS be improved. [Contribute your thoughts](/movement/publish-howto).

