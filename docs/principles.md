---
layout: default
title: Principles of Social Coding
nav_order: 2
permalink: /principles
---

# The Principles and Practices of Social Coding

## Creating CODE GEM's

At the heart of Social Coding are four universal principles:

1. [**C**ommunity first](#community-first)
2. [**O**pen is everything](#open-is-everything)
3. [**D**emocratic governance](#democratic-governance)
4. [**E**ducate and learn](#educate-and-learn)

The first characters form the acronym 'CODE' and we will refer to them as CODE Principles from now on. While the Principles address _how_ we go about to build our software, and the core Practices of Social Coding are concerned with _what_ we should focus on to deliver:

1. [**G**row affinity](#grow-affinity)
2. [**E**volve the commons](#evolve-the-commons)
3. [**M**onitor externalities](#monitor-externalities)

And conveniently these are then the 'GEM' acronym. The GEM Values that assure that free software addresses people's needs.

## Four basic principles

### Community first

Social coding revolves around [communities of action](https://en.wikipedia.org/wiki/Community_of_action). Software is made by people for people in a highly collaborative and creative process. Deep involvement of participants and their social interactions are key to success.

### Open is everything

Social coding is about working in public and in full transparency. Projects are built with open technologies and based on open standards. We embrace the notion of [FLO](https://wiki.snowdrift.coop/about/free-libre-open#flo), Free / Libre / Open, for all the work we do.

### Democratic governance

Social coding is for everyone. Regardless of skills and background every person can have their say and make their voices heard. Communities are self-governed and apply democratic principles that foster inclusion and diversity.

### Educate and learn

Social coding recognizes the breadth of its field. Every person has unique skills and traits. We are open to learn from each other, and will actively teach those who want to be taught. We encourage lifelong learning, and thinking out-of-the-box.

## Three core practices

### Grow affinity

Our software must address real human needs. It should be in support of people's day-to-activites and an enrichment to their lives. We strive to deliver software that people will come to love. This does not happen all at once. We aim to grow software incrementally such that people's affinity grows with it simultaneously.

### Evolve the commons

Free software in general should be to the benefit of the commons. This starts by adhering to the [Four Essential Freedoms](https://en.wikipedia.org/wiki/The_Free_Software_Definition#The_Four_Essential_Freedoms_of_Free_Software) and more broadly to [FLO](https://wiki.snowdrift.coop/about/free-libre-open#flo) principles. We take into account prior works that exist and strive to build on top of their foundations, so that the commons as a whole benefits to maximum extent. We give back to the works we use.

### Monitor externalities

In all the decisions that take place along the free software development lifecycle the possible externalities, both positive and negative should be investigated and handled properly. Positive externalities may lead to new opportunities, whereas negative externalities must be mitigated in best ways possible.

## Practice what we preach

Of course we strive to adhere to CODE principles and GEM practices ourself. We intend to elaborate them along the way in some of our [Ecosystem projects](/ecosystem/projects) and [Communities](/ecosystem/communities). We will not be perfect at this from the very start, but we consider the projects we engage in as reference implementations of [Social Coding practices](/methodology/practitioners). We'll eat our own dog food, and hopefully we will deliver CODE GEM's that are delightful.

